INCLUDE_DIRS = -I../../include -I/usr/include
CXXFLAGS += -O3 -DLINUX -Wall $(INCLUDE_DIRS)
target = svm
object = svm.cpp
opencv_lib = -L"/usr/local/lib" -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_objdetect -lopencv_ml -lopencv_imgcodecs -lopencv_videoio
$(target):$(object)
	g++ $(CXXFLAGS) $(object) -o $(target) $(opencv_lib) 
